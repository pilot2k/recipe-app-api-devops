variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "dzmitry@antonau.com"
}

variable "db_username" {
  description = "Database username"

}

variable "db_password" {
  description = "Database password"
}

variable "bastion_key_name" {
  default = "recipe-app-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "533266987121.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "533266987121.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "secret key for django app"
}